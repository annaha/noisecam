let xoff1 = 0
let xoff2 = 10000
let capture
let vekt

function setup(){
	let canv = createCanvas(window.innerWidth, window.innerHeight)
	capture = createCapture(VIDEO)
	capture.elt.setAttribute('playsinline', '')
	capture.size(45, 45)
	capture.hide()
	vekt = createVector(0,0)
}

function draw(){
	vekt.x = noise(xoff1) * width
	vekt.y = noise(xoff2) * height
	xoff1 += 0.003
	xoff2 += 0.003
	noStroke()
	tint(255, 128)
		if ( vekt.x > height){
			vekt.x = vekt.x - height
		}
		if (vekt.y > width){
			vekt.y = vekt.y -width
		}
	smooth()
	image(capture, vekt.x, vekt.y, 25, 25)
    smooth()
	image(capture, vekt.y, vekt.x, 25, 25)
	keret()
}

function keret(){
	noFill()
	stroke(155)
	strokeWeight(5, 0.9)
	beginShape();
	vertex(0,0);
	vertex(0, width);
	vertex(height+25, width);
	vertex(height+25, 0);
	endShape(CLOSE);
}
	